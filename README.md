# Internationalizer
A mime to Netbeans strings internationaliser
it gets all the strings in a view and propose to internationalize it in various languages

<table caption="Example">
<thead>
<tr>
<td>Key</td>
<td>English</td>
<td>Chinese</td>
<td>Arab</td>
<td>French</td>
</tr>
</thead>
<tbody>
<tr>
<td>HOME</td>
<td>Home</td>
<td>家</td>
<td>-</td>
<td>Maison</td>
</tr>
</tbody>
</table>